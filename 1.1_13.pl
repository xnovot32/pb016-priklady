% nacteni:
/* ['1.1.1_13.pl']. */

% fakty (DB)
otec(milan,dana).
otec(milan,petr).
otec(jan,david).

matka(pavla,dana).
matka(pavla,petr).
matka(jana,david).

% pravidla
rodic(X,Y):- otec(X,Y).
rodic(X,Y):- matka(X,Y).

sourozenci(X,Y):- otec(O,X), otec(O,Y), X\=Y, matka(M,X), matka(M,Y).

% demonstracni vypis

  % abychom se vyhli varovanim "Redefined static procedure ..."
:- dynamic
       write_all_X/3,
       start/0.
       
write_all_X(Goal,X,Name):-
            call(Goal),write('  '),write(Name),write(' = '),write(X),nl,fail.
write_all_X(_,_,_).

start:-
    write('Program predstavuje jednoduchou prologovskou databazi'),nl,
    write('s fakty i pravidly o rodinnych vztazich "otec", "matka"'),nl,
    write('a "rodic".'),nl,nl,
    write('Na databazi muzeme klast pomoci volani prologovskych cilu'),nl,
    write('ruzne dotazy. Napr.:'),nl,nl,
    write('Vysledek dotazu "otec(X,dana)" je:'),nl,
    write_all_X(otec(X,dana),X,'X'),nl,
    write('Vysledky dotazu "rodic(X,david)" jsou: '),nl,
    write_all_X(rodic(X,david),X,'X'),nl,
    write('Vysledek dotazu "sourozenci(dana,Y)" je:'),nl,
    write_all_X(sourozenci(dana,Y),Y,'Y'),nl,
    write('Sami si vyzkousejte dalsi kombinace.'),nl.
?-start.

:- retractall(write_all_X/3).
:- retractall(start/0).
