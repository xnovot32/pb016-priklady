% nacteni:
/* ['1.2_16.pl']. */

tiskniseznam(S):-write('seznam=['),nl,tiskniseznam(S,1).
tiskniseznam([],_):-write(']'),nl.
tiskniseznam([H|T],N):- tab(4),write(N),write(': '),write(H),nl,N1 is N+1,tiskniseznam(T,N1).

% demonstracni vypis

  % abychom se vyhli varovanim "Redefined static procedure ..."
:- dynamic
       start/0.
start:-
    write('Program vytiskne zadany seznam. Napr.:'),nl,nl,
    write('Vysledek dotazu "tiskniseznam([a, b, c])." je:'),nl,
    tiskniseznam([a, b, c]),nl,
    write('Vysledek dotazu "tiskniseznam([a, b, [c1, c2, c3], d, `mezi d a e`, e])." je:'),nl,
    tiskniseznam([a, b, [c1, c2, c3], d, 'mezi d a e', e]).
?-start.

:- retractall(write_all_X/3).
:- retractall(start/0).
