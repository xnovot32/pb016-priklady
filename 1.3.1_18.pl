% nacteni:
/* ['1.3.1_18.pl']. */

fib(0,0).
fib(1,1).
fib(X,Y) :- X1 is X-1, X2 is X-2, fib(X1,Y1), fib(X2,Y2), Y is Y1+Y2.

% demonstracni vypis

  % abychom se vyhli varovanim "Redefined static procedure ..."
:- dynamic
       write_all_X/3,
       start/0.
       
write_all_X(Goal,X,Name):-
            call(Goal),write('  '),write(Name),write(' = '),write(X),nl,fail.
write_all_X(_,_,_).

start:- 
    write('Program pro vypocet clenu fibonacciho posloupnosti, '),nl,
    write('mene efektivni varianta'),nl,
    write('Napr.:'),nl,
    write('Vypocet potrva nekolik vterin, '), 
    write('Vysledek volani "fib(27, Vysledek)." je:'),nl,
    fib(27, Vysledek), write('  Vysledek = '), write(Vysledek),nl.   
?-start.

:- retractall(write_all_X/3).
:- retractall(start/0).
