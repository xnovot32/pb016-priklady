% nacteni:
/* ['1.3.2_19.pl']. */

% predikat fib musi byt dynamicky (kvuli asserta)
:- dynamic fib/2.

fib(0,0).
fib(1,1).
fib(X,Y) :- X1 is X-1, X2 is X-2, fib(X1,Y1), fib(X2,Y2), Y is Y1+Y2, asserta(fib(X,Y)).

% demonstracni vypis

  % abychom se vyhli varovanim "Redefined static procedure ..."
:- dynamic
       start/0.
       
start:- 
    write('Program pro vypocet clenu fibonacciho posloupnosti, '),nl,
    write('efektivnejsi varianta'),nl,
    write('Napr.:'),nl,
    write('Vysledek volani "fib(27, Vysledek)." je:'),nl,
    fib(27, Vysledek), write('  Vysledek = '), write(Vysledek),nl,   
    write('Pomoci volani "listing." se mj. muzete podivat na fakty '),nl,
    write('ulozene do databaze pomoci asserta.'),nl.
?-start.

:- retractall(start/0).
