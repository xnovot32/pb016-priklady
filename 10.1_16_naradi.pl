% příklad na klasifikaci předmětů podle jejich obrysu

attribute( velikost, [ mala, velka]).
attribute( tvar, [ podlouhly, kompaktni, jiny]).
attribute( diry, [ zadne, 1, 2, 3, hodne]).

% Rozpoznávání předmětů

example( matice, [ velikost = mala, tvar = kompaktni, diry = 1]).
example( sroub, [ velikost = mala, tvar = podlouhly, diry = zadne]).
example( klic, [ velikost = mala, tvar = podlouhly, diry = 1]).
example( matice, [ velikost = mala, tvar = kompaktni, diry = 1]).
example( klic, [ velikost = velka, tvar = podlouhly, diry = 1]).
example( sroub, [ velikost = mala, tvar = kompaktni, diry = zadne]).
example( matice, [ velikost = mala, tvar = kompaktni, diry = 1]).
example( tuzka, [ velikost = velka, tvar = podlouhly, diry = zadne]).
example( nuzky, [ velikost = velka, tvar = podlouhly, diry = 2]).
example( tuzka, [ velikost = velka, tvar = podlouhly, diry = zadne]).
example( nuzky, [ velikost = velka, tvar = jiny, diry = 2]).
example( klic, [ velikost = mala, tvar = jiny, diry = 2]).

% vim: set ft=prolog:
