% p��klad na klasifikaci po�as�

attribute(obloha,  [slunecno, zamraceno, dest]).
attribute(teplota, [teplo, mirna, chladno]).
attribute(vlhkost, [vysoka, normalni]).
attribute(vitr,    [slaby, silny]).

% Kdy je dobre pocasi pro hru golfu?

example(spatne_pocasi, [obloha = slunecno, teplota = teplo, vlhkost = vysoka, vitr = slaby]).
example(spatne_pocasi, [obloha = slunecno, teplota = teplo, vlhkost = vysoka, vitr = silny]).
example(dobre_pocasi,  [obloha = oblacno, teplota = teplo, vlhkost = vysoka, vitr = slaby]).
example(dobre_pocasi,  [obloha = dest, teploty = mirna, vlhkost = vysoka, vitr = slaby]).
example(dobre_pocasi,  [obloha = dest, teploty = chladno, vlhkost = normalni, vitr = slaby]).

example(spatne_pocasi, [obloha = dest, teploty = chladno, vlhkost = normalni, vitr = silny]).
example(dobre_pocasi,  [obloha = oblacno, teplota = chladno, vlhkost = normalni, vitr = silny]).
example(spatne_pocasi, [obloha = slunecno, teplota = mirna, vlhkost = vysoka, vitr = slaby]).
example(dobre_pocasi,  [obloha = slunecno, teplota = chladno, vlhkost = normalni, vitr = slaby]).
example(dobre_pocasi,  [obloha = dest, teploty = mirna, vlhkost = normalni, vitr = slaby]).

example(dobre_pocasi,  [obloha = slunecno, teplota = mirna, vlhkost = normalni, vitr = silny]).
example(dobre_pocasi,  [obloha = oblacno, teplota = mirna, vlhkost = vysoka, vitr = silny]).
example(dobre_pocasi,  [obloha = oblacno, teplota = teplo, vlhkost = normalni, vitr = slaby]).
example(spatne_pocasi, [obloha = dest, teploty = mirna, vlhkost = vysoka, vitr = silny]).

% vim: set ft=prolog:
