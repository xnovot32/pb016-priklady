% p��klad na rozhodnut�, jestli po�kat na st�l v restauraci
%
attribute( alt, [ano, ne]).
attribute( bar, [ano, ne]).
attribute( paso, [ano, ne]).
attribute( hlad, [ano, ne]).
attribute( stam, [nikdo, cast, plno]).
attribute( cen, ['$', '$$', '$$$']).
attribute( dest, [ano, ne]).
attribute( rez, [ano, ne]).
attribute( typ, [mexicka, asijska, bufet, pizzerie]).

example(pockat,  [alt=ano, bar=ne,  paso=ne,  hlad=ano, stam=cast,  cen='$$$', dest=ne,  rez=ano, typ=mexicka ]).
example(necekat, [alt=ano, bar=ne,  paso=ne,  hlad=ano, stam=plno,  cen='$',   dest=ne,  rez=ne,  typ=asijska ]).
example(pockat,  [alt=ne,  bar=ano, paso=ne,  hlad=ne,  stam=cast,  cen='$',   dest=ne,  rez=ne,  typ=bufet   ]).
example(pockat,  [alt=ano, bar=ne,  paso=ano, hlad=ano, stam=plno,  cen='$',   dest=ne,  rez=ne,  typ=asijska ]).
example(necekat, [alt=ano, bar=ne,  paso=ano, hlad=ne,  stam=plno,  cen='$$$', dest=ne,  rez=ano, typ=mexicka ]).
example(pockat,  [alt=ne,  bar=ano, paso=ne,  hlad=ano, stam=cast,  cen='$$',  dest=ano, rez=ano, typ=pizzerie]).
example(necekat, [alt=ne,  bar=ano, paso=ne,  hlad=ne,  stam=nikdo, cen='$',   dest=ano, rez=ne,  typ=bufet   ]).
example(pockat,  [alt=ne,  bar=ne,  paso=ne,  hlad=ano, stam=cast,  cen='$$',  dest=ano, rez=ano, typ=asijska ]).
example(necekat, [alt=ne,  bar=ano, paso=ano, hlad=ne,  stam=plno,  cen='$',   dest=ano, rez=ne,  typ=bufet   ]).
example(necekat, [alt=ano, bar=ano, paso=ano, hlad=ano, stam=plno,  cen='$$$', dest=ne,  rez=ano, typ=pizzerie]).
example(necekat, [alt=ne,  bar=ne,  paso=ne,  hlad=ne,  stam=nikdo, cen='$',   dest=ne,  rez=ne,  typ=asijska ]).
example(pockat,  [alt=ano, bar=ano, paso=ano, hlad=ano, stam=plno,  cen='$',   dest=ne,  rez=ne,  typ=bufet   ]).

% vim: set ft=prolog:
