% nacteni:
/* ['12.1_11.pl']. */

% 1. cast -- pravidla
sentence --> noun_phrase, verb_phrase.
noun_phrase --> determiner, noun_phrase2.
noun_phrase --> noun_phrase2.
noun_phrase2 --> adjective, noun_phrase2.
noun_phrase2 --> noun.
verb_phrase --> verb.
verb_phrase --> verb, noun_phrase.

% 2. cast -- lexikon
determiner --> [the].
determiner --> [a]. 
noun --> [boy].
noun --> [song].
verb --> [sings].
adjective --> [young].
