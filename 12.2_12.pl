% nacteni:
/* ['12.2_12.pl']. */

% vetu reprezentujeme seznamem slov [the,young,boy,sings,a,song]
% pravidlova cast
sentence(S) :- append(NP,VP,S),
    noun_phrase(NP), verb_phrase(VP).
    % ...
  
% slovnikova cast, lexikon � zapisujeme pomoci faktu:
determiner([the]).
determiner([a]). 
noun([boy]).
    % ...
  
