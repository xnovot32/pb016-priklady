% nacteni:
/* ['12.3_13.pl']. */

sentence(S,S0) :- noun_phrase(S,S1), verb_phrase(S1,S0).

noun_phrase(S,S0) :- determiner(S,S1), noun_phrase2(S1,S0).
noun_phrase(S,S0) :- noun_phrase2(S,S0).
noun_phrase2(S,S0) :- adjective(S,S1), noun_phrase2(S1,S0).
noun_phrase2(S,S0) :- noun(S,S0).
verb_phrase(S,S0) :- verb(S,S0).
verb_phrase(S,S0) :- verb(S,S1), noun_phrase(S1,S0).

determiner([the|S],S).
determiner([a|S],S).
verb([sings|S],S).
noun([boy|S],S).
noun([song|S],S).
adjective([ young|S],S).

start:- 
    write('Gramatiky - rozdilove seznamy'),nl,nl,
    write('Zkuste si zadat dotaz "sentence([the,young,boy,sings,a,song],[])"').    
?-start.

:- retractall(start/0).
