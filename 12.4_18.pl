% nacteni:
/* ['12.4_18.pl']. */


sentence(s(N,V)) --> noun_phrase(N), verb_phrase(V).
noun_phrase(np(D,N)) --> determiner(D), noun_phrase2(N).
noun_phrase(np(N)) --> noun_phrase2(N).
noun_phrase2(np2(A,N)) --> adjective(A), noun_phrase2(N).
noun_phrase2(np2(N)) --> noun(N).
verb_phrase(vp(V)) --> verb(V).
verb_phrase(vp(V,N)) --> verb(V), noun_phrase(N).
determiner(det(the)) --> [the].
determiner(det(a)) --> [a].
adjective(adj(young)) --> [young].
noun(noun(boy)) --> [boy].
noun(noun(song)) --> [song].
verb(verb(sings)) --> [sings].

% demonstracni vypis

  % abychom se vyhli varovanim "Redefined static procedure ..."
:- dynamic
       write_all_X/3,
       start/0.
       
write_all_X(Goal,X,Name):-
            call(Goal),write('  '),write(Name),write(' = '),write(X),nl,fail.
write_all_X(_,_,_).

start:- 
    write('DC GRAMATIKA S KONSTRUKCI STROMU ANALYZY'),nl,nl,
    write('Dotaz "sentence(Tree, [the,young,boy,sings,a,song],[])" :'),nl,
    write_all_X(sentence(Tree, [the,young,boy,sings,a,song],[]), Tree, 'Tree').

?-start.

:- retractall(start/0).
:- retractall(write_all_X/3).
