% nacteni:
/* ['12.5_22.pl']. */

sentence(sentence(N,V)) --> noun_phrase(N,Num), verb_phrase(V,Num).
noun_phrase(np(D,N),Num) --> determiner(D,Num), noun_phrase2(N,Num).
noun_phrase(np(N),Num) --> noun_phrase2(N,Num).
noun_phrase2(np2(A,N),Num) --> adjective(A), noun_phrase2(N,Num).
noun_phrase2(np2(N),Num) --> noun(N,Num).
verb_phrase(vp(V),Num) --> verb(V,Num).
verb_phrase(vp(V,N),Num) --> verb(V,Num), noun_phrase(N,_Num1).

determiner(det(the),_) --> [the].
determiner(det(a),sg) --> [a].
verb(verb(sings),sg) --> [sings].
verb(verb(sing),pl ) --> [sing].
adjective(adj(young)) --> [young].
noun(noun(boy),sg) --> [boy].
noun(noun(song),sg) --> [song].
noun(noun(boys),pl) --> [boys].
noun(noun(songs),pl) --> [songs].

:- dynamic
       start/0,
       start2/0,
       start3/0.

start:- 
    write('DC GRAMATIKA S TESTY NA SHODU'),nl,nl,
    write('Dotaz "sentence(_ ,[a, young, boys, sings ],[])" vrati:'),nl,
    sentence(_ ,[a, young, boys, sings ],[]).
?-start.

start2:-
    write('/* No */)'),nl, 
    nl,write('Dotaz "ssentence(_,[the,boys,sings,a,song ],[])" vrati:'),nl,
    sentence(_,[the,boys,sings,a,song ],[]).
?-start2.

start3:- 
    write('/* No */'),nl,
    nl,write('Dotaz "sentence(_,[the,boys,sing,a,song ],[])" vrati:'),nl,
    sentence(_,[the,boys,sing,a,song ],[]),
    write('/* Yes */'). 
?-start3.

:- retractall(start/0).
:- retractall(start2/0).
:- retractall(start3/0).
