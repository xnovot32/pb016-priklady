% nacteni:
/* ['12.7_24.pl']. */

abc --> a(N), b(N), c(N).
a(0) --> [].
a(s(N)) --> [a], a(N).
b(0) --> [].
b(s(N)) --> [b], b(N).
c(0) --> [].
c(s(N)) --> [c], c(N).

% demonstracni vypis

:- dynamic
       write_all_X/3,
       start/0.
       
write_all_X(Goal,X,Name):-
            call(Goal),write('  '),write(Name),write(' = '),write(X),nl,fail.
write_all_X(_,_,_).

start:- 
    write('Generativni sila DCG'),nl,nl,
    write('jazyk a^n b^n c^n'),nl,
    write('Zkuste si zadat dotaz "abc(X,[])" (dalsi reseni si vyzadate '), 
    write('stiskem ";").'),nl.
?-start.

:- retractall(write_all_X/3).
:- retractall(start/0).
