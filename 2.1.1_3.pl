% nacteni:
/* ['2.1.1_3.pl']. */

% vymaz predchozi deklarace member
:- retractall(member/2).
:- dynamic member/2.

member(X,[X|_]).
member(X,[_|T]) :- member(X,T).

% demonstracni vypis

  % abychom se vyhli varovanim "Redefined static procedure ..."
:- dynamic
       write_all_X/3,
       start/0.
       
write_all_X(Goal,X,Name):-
            call(Goal),write('  '),write(Name),write(' = '),write(X),nl,fail.
write_all_X(_,_,_).

start:- 
    write('Member - 1. varianta'),nl,
    write('Vysledek volani "member(a,[X,b,c])" je:'),nl,
    write_all_X(member(a,[X,b,c]), X, 'X').    
?-start.

:- retractall(write_all_X/3).
:- retractall(start/0).
