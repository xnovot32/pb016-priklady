% nacteni:
/* ['2.1.3_3.pl']. */

% vymaz predchozi deklarace member
:- retractall(member/2).
:- dynamic member/2.

member(X,[Y|_]) :- X == Y.
member(X,[Y|T]) :- X \== Y, member(X,T).

% demonstracni vypis

  % abychom se vyhli varovanim "Redefined static procedure ..."
:- dynamic
       write_all_X/3,
       start/0.
       
write_all_X(Goal,X,Name):-
            call(Goal),write('  '),write(Name),write(' = '),write(X),nl,fail.
write_all_X(_,_,_).

start:- 
    write('Member - 3. varianta (efektivnejsi, taktez ne obousmerna)'),nl, nl,
    write('Vysledek volani "member(a,[X,b,c])" je:'),nl,
    write_all_X(member(a,[X,b,c]), X, 'X'),
    write('No'), nl, nl,
    write('Vysledek volani "member(a,[a,b,a]),write(ok),nl,fail"'), nl, 
    write('  (konci pri nalezeni 1. vyskytu "a")  je:'),nl.
?-start.
?-member(a,[a,b,a]),write(ok),nl,fail.


:- retractall(write_all_X/3).
:- retractall(start/0).

/* member_demo(a,[a,b,a]),write(ok),nl, fail . */
% vrati:
%   ok
%   No
