% nacteni:
/* ['2.10_23.pl']. */

stree(Graph,Tree) :- member(Edge,Graph),spread([Edge],Tree,Graph).
spread(Tree1,Tree,Graph) :- addedge(Tree1,Tree2,Graph),spread(Tree2,Tree,Graph).
spread(Tree,Tree,Graph) :- \+ addedge(Tree,_,Graph).
addedge(Tree,[A-B|Tree],Graph) :- adjacent(A,B,Graph),node(A,Tree), \+ node(B,Tree).
adjacent(A,B,Graph) :- member(A-B,Graph);member(B-A,Graph).
node(A,Graph) :- adjacent(A,_,Graph), !. % bez rezu cil uspeje az deg(G)-krat, coz nam potom generuje identicke kostry.

% demonstracni vypis

  % abychom se vyhli varovanim "Redefined static procedure ..."
:- dynamic
       write_all_X/3,
       start/0.

write_all_X(Goal,X,Name):-
            call(Goal),write('  '),write(Name),write(' = '),write(X),nl,fail.
write_all_X(_,_,_).

start:- 
    write('Kostra grafu'),nl,
    write('Volani "stree([a-b,b-c,b-d,c-d],T)" vrati:'),nl,
    write_all_X(stree([a-b,b-c,b-d,c-d],T), T, 'T').

?-start.

:- retractall(write_all_X/3).
:- retractall(start/0).
