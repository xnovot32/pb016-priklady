% nacteni:
/* ['2.2_4.pl']. */

del(_,[],[]).
del(A,[A|T],V) :- del(A,T,V).
del(A,[H|T1],[H|T2]) :- A\=H, del(A,T1,T2).

del1(A,[A|T],T).
del1(A,[H|T1],[H|T2]) :- del1(A,T1,T2).

insert(A,L,[A|L]).
insert(A,[H|T1],[H|T2]):- insert(A,T1,T2).

insert1(X,List ,[X|List]).

% demonstracni vypis

  % abychom se vyhli varovanim "Redefined static procedure ..."
:- dynamic
       write_all_X/3,
       start/0.
       
write_all_X(Goal,X,Name):-
            call(Goal),write('  '),write(Name),write(' = '),write(X),nl,fail.
write_all_X(_,_,_).

start:- 
    write('Prace se seznamy - del a insert'),nl,
    write('-------------------------------'),nl, nl,
    write('predikat del(A,L,Vysl) smaze vsechny vyskyty prvku A ze seznamu L'),nl,    
    write('Vysledek volani "del (1,[1,2,1,1,2,3,1,1], L)":'),nl,
    write_all_X(del(1,[1,2,1,1,2,3,1,1], L), L, 'L'),nl,

    write('del1(A,L,Vysl) smaze vzdy jeden (podle poradi) vyskyt prvku A v seznamu L'),nl,
    write('Vysledek volani "del1(1,[1,2,1],L)":'),nl,
    write_all_X(del1(1,[1,2,1],L), L, 'L'),nl,

    write('insert(A,L,Vysl) vklada postupne (pri zadosti o dalsi reseni) na vsechny pozice seznamu L prvek A'),nl,
    write('Vysledek volani "insert(4,[2,3,1], L)":'),nl,
    write_all_X(insert(4,[2,3,1], L), L, 'L'),nl,

    write('insert1(A,L,Vysl) vlozi A na zacatek seznamu L'),nl,
    write('Vysledek volani "insert1(4,[2,3,1], L)":'),nl,
    write_all_X(insert1(4,[2,3,1], L), L, 'L').
?-start.

:- retractall(write_all_X/3).
:- retractall(start/0).
