% nacteni:
/* ['2.4.1_6.pl']. */

append([],L,L).
append([H|T1],L2,[H|T]) :- append(T1,L2,T).

% demonstracni vypis

  % abychom se vyhli varovanim "Redefined static procedure ..."
:- dynamic
       write_all_X/3,
       start/0.
       
write_all_X(Goal,X,Name):-
            call(Goal),write('  '),write(Name),write(' = '),write(X),nl,fail.
write_all_X(_,_,_).

write_all_XY(Goal,X,XName,Y,YName):-
            call(Goal),write('  '),write(XName),write(' = '),write(X),
                       write('  '),write(YName),write(' = '),write(Y),
                       nl, fail.
write_all_XY(_,_,_,_,_).

start:- 
    write('Prace se seznamy - append'),nl,
    write('-------------------------------'),nl, nl,
    write('vicesmernost predikatu append:'),nl,    
    write('Vysledek volani "append([a,b],[c,d],L)":'),nl,
    write_all_X(append([a,b],[c,d],L), L, 'L'),nl,

    write('Vysledek volani "append(X,[c,d],[a,b,c,d ]).":'),nl,
    write_all_X(append(X,[c,d],[a,b,c,d ]), X, 'X'),nl,

    write('Vysledek volani "append(X,Y,[a,b,c])":'),nl,
    write_all_XY(append(X,Y,[a,b,c]), X, 'X', Y, 'Y'),nl.

?-start.

:- retractall(write_all_X/3).
:- retractall(start/0).
