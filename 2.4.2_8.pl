% nacteni:
/* ['2.4.2_8.pl']. */

append_dl(A-B,B-C,A-C).

% demonstracni vypis

  % abychom se vyhli varovanim "Redefined static procedure ..."
:- dynamic
       start/0.
       
start:- 
    write('Prace se seznamy - efektivita append'),nl,nl,
    write('Zkuste si zadat dotaz "append_dl([a,b|X]-X,[c,d|Y]-Y,Z)":'),nl.
?-start.


:- retractall(start/0).
