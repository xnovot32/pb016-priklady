% nacteni:
/* ['2.5.1_10.pl']. */

:- retractall(start/0).
:- retractall(qsort/2).
:- retractall(divide/4).
:- dynamic
       qsort/2,
       divide/4.

qsort([],[]) :- !.
qsort([H],[H]) :- !.
qsort([H|T],L) :- divide(H,T,M,V),
                  qsort(M,M1), qsort(V,V1),
                  append(M1,[H|V1],L).
divide(_,[],[],[]) :- !.
divide(H,[K|T],[K|M],V) :- K=<H, !, divide(H,T,M,V).
divide(H,[K|T],M,[K|V]) :- K>H, divide(H,T,M,V).

% demonstracni vypis

  % abychom se vyhli varovanim "Redefined static procedure ..."
:- dynamic
       start/0.
       
start:- 
    write('Radici algoritmus QuickSort'),nl,nl,
    write('Vysledek volani "qsort([5, 2, 8, 2, 654, 8, 3, 4], L)":'),nl,
    qsort([5, 2, 8, 2, 654, 8, 3, 4], L), write('L = '),write(L),nl.

?-start.

:- retractall(start/0).
