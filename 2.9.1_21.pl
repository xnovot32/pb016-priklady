% nacteni:
/* ['2.9.1_21.pl']. */


path(A,Z,Graph,Cesta) :- path1(A,[Z],Graph,Cesta).

path1(A,[A|Cesta1],_,[A|Cesta1]).
path1(A,[Y|Cesta1],Graph,Cesta) :- adjacent(X,Y,Graph),
    \+ member(X,Cesta1),path1(A,[X,Y|Cesta1],Graph,Cesta).

adjacent(X,Y,graph(_,Edges)) :- member(e(X,Y),Edges)
                                    ;member(e(Y,X),Edges).

graph([a,b,c,d],[e(a,b),e(b,d),e(b,c),e(c,d)]).

% demonstracni vypis

start:- 
    write('Cesta v grafu'),nl, nl,
    write('path(a, c, graph, Cesta) : '),
    graph(Nodes,Edges),
    path(a, c, graph(Nodes,Edges), Cesta),
    write(Cesta), nl, nl.

?-start.

