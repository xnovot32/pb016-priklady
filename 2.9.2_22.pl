% nacteni:
/* ['2.9.2_22.pl']. */

path(A,Z,Graf,Cesta,Cena) :- path1(A,[Z],0,Graf,Cesta,Cena).

path1(A,[A|Cesta1],Cena1,_,[A|Cesta1],Cena1).
path1(A,[Y|Cesta1],Cena1,Graf,Cesta,Cena) :- adjacent(X,Y,CenaXY,Graf),
    \+ member(X,Cesta1),Cena2 is Cena1+CenaXY,
    path1(A,[X,Y|Cesta1],Cena2,Graf,Cesta,Cena).
    
adjacent(X,Y,CenaXY,Graf) :- member(X-Y/CenaXY,Graf);member(Y-X/CenaXY,Graf).

start:- 
    write('Cesta v grafu'),nl, nl,
    path(s, u, [s-t/3, t-v/1, t-u/5, u-t/2, v-u/2], Cesta, Cena),
    write('Cesta='), write(Cesta), nl,
    write('Cena='), write(Cena), nl, nl.

?-start.
