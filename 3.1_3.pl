% nacteni:
/* ['3.1_3.pl']. */

:- retractall(solution/1).
:- retractall(sol/1).
:- retractall(noattack/2).
:- retractall(template/1).
       
:- dynamic
       solution/1,
       sol/1,
       noattack/2,
       template/1.


solution(S) :- template(S), sol(S).

sol([]).
sol([X/Y|Others]) :- sol(Others),
                      member(X,[1,2,3,4,5,6,7,8]),
                      member(Y,[1,2,3,4,5,6,7,8]),
                      noattack(X/Y,Others).

noattack(_,[]).
noattack(X/Y,[X1/Y1|Others]) :- X=\=X1, Y=\=Y1, Y1-Y=\=X1-X, Y1-Y=\=X-X1,
                                noattack(X/Y,Others).

template([_X1/_Y1, _X2/_Y2, _X3/_Y3, _X4/_Y4, _X5/_Y5, _X6/_Y6, _X7/_Y7, _X8/_Y8]).

% demonstracni vypis

  % abychom se vyhli varovanim "Redefined static procedure ..."
:- dynamic
       start/0.

start:- 
    write('PROBLEM OSMI DAM I'),nl,
    write('Zadejte dotaz "solution(Solution)", po vypoctu prvniho reseni'),nl,
    write('si dalsi vyzadate stisknutim ";".'),nl.
?-start.

:- retractall(start/0).
