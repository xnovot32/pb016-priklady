% nacteni:
/* ['3.2_4.pl']. */

:- retractall(solution/1).
:- retractall(sol/1).
:- retractall(noattack/2).
:- retractall(template/1).
       
:- dynamic
       solution/1,
       sol/1,
       noattack/2,
       template/1.

solution(S) :- template(S), sol(S).

sol([]).
sol([X/Y|Others]) :- sol(Others),
                      % member(X, ...) zde nemusi byt, protoze X je jiz
                      % pevne nastavena v template. Ve slajdech take neni.
                      member(Y,[1,2,3,4,5,6,7,8]),
                      noattack(X/Y,Others).

noattack(_,[]).
noattack(X/Y,[X1/Y1|Others]) :- X=\=X1, Y=\=Y1, Y1-Y=\=X1-X, Y1-Y=\=X-X1,
                                noattack(X/Y,Others).

template([1/_Y1,2/_Y2,3/_Y3,4/_Y4,5/_Y5,6/_Y6,7/_Y7,8/_Y8]).

% demonstracni vypis

  % abychom se vyhli varovanim "Redefined static procedure ..."
:- dynamic
       start/0.

start:- 
    write('PROBLEM OSMI DAM II - jeden sloupec pro kazdou damu'),nl,
    write('Zadejte dotaz "solution(Solution)", po vypoctu prvniho reseni'),nl,
    write('si dalsi vyzadate stisknutim ";" (vsimnete si zrychleni oproti'),nl,
    write(' verzi I).'),nl.
?-start.

:- retractall(start/0).
