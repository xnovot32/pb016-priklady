% nacteni:
/* ['3.3_5.pl']. */

:- retractall(solution/1).
:- retractall(sol/1).
:- retractall(noattack/2).
:- retractall(template/1).
       
:- dynamic
       solution/1,
       sol/1,
       noattack/2,
       template/1.

solution(YList) :- sol(YList ,[1,2,3,4,5,6,7,8],[1,2,3,4,5,6,7,8],
                       [-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7],
                       [2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]).
sol([],[], _Dy,_Du,_Dv).
sol([Y|YList],[X|Dx1],Dy,Du,Dv) :- del(Y,Dy,Dy1), U is X-Y, del(U,Du,Du1), V is X+Y,
                                   del(V,Dv,Dv1), sol(YList,Dx1,Dy1,Du1,Dv1).
del(Item,[ Item|List ], List ).
del(Item,[ First | List ],[ First | List1 ]) :- del(Item,List , List1 ).

% demonstracni vypis

  % abychom se vyhli varovanim "Redefined static procedure ..."
:- dynamic
       start/0.

start:- 
    write('PROBLEM OSMI DAM III - reseni pomoci seznamu volnych pozic'),nl,
    write('Zadejte dotaz "solution(Solution)", po vypoctu prvniho reseni'),nl,
    write('si dalsi vyzadate stisknutim ";".'),nl.
?-start.

:- retractall(start/0).
