% nacteni:
/* ['3.4_13.pl']. */

solution(Node,Solution) :- depth_first_search([], Node,Solution).

depth_first_search(Path,Node,[Node|Path]) :- goal(Node).
depth_first_search(Path,Node,Sol) :- move(Node,Node1),
    \+ member(Node1,Path),depth_first_search([Node|Path],Node1,Sol).
