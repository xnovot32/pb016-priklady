% nacteni:
/* ['3.5_15.pl']. */

solution(Node,Solution) :- depth_first_search_limit(Node,Solution,999).

depth_first_search_limit(Node,[Node], _) :- goal(Node).
depth_first_search_limit(Node,[Node|Sol],MaxDepth) :- MaxDepth>0, move(Node,Node1),
    Max1 is MaxDepth-1,depth_first_search_limit(Node1,Sol,Max1).
