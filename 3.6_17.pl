% nacteni:
/* ['3.6_17.pl']. */

solution(Start ,Solution) :- breadth_first_search([[ Start ]], Solution).

breadth_first_search([[ Node|Path]|_],[Node|Path]) :- goal(Node).
breadth_first_search([[ N|Path]|Paths],Solution) :-
    bagof([M,N|Path],(move(N,M),\+ member(M,[N|Path])), NewPaths),
    NewPaths\=[], append(Paths,NewPaths,Path1), !,
    breadth_first_search(Path1,Solution); breadth_first_search(Paths,Solution).
