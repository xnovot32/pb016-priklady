% nacteni:
/* ['5.1_5.pl']. */

:- retractall(start/0).
:- retractall(write_all_X/3).

?-op(100,xfx,to), dynamic(hanoi/5).

hanoi(1,A,B,_,[A to B]).
hanoi(N,A,B,C,Moves) :- N>1, N1 is N-1, lemma(hanoi(N1,A,C,B,Ms1)),
hanoi(N1,C,B,A,Ms2), append(Ms1,[A to B|Ms2],Moves).

lemma(P) :- P,asserta((P :- !)).

% demonstracni vypis

  % abychom se vyhli varovanim "Redefined static procedure ..."
:- dynamic
       write_all_X/3,
       start/0.
       
write_all_X(Goal,X,Name):-
            call(Goal),write('  '),write(Name),write(' = '),write(X),nl,fail.
write_all_X(_,_,_).

start:- 
    write('Demostrace programu Hanoi'),nl,
    write('hanoi(+PocetTaliru,+z_tyce,+na_tyc,+pomoci_tyce,-Tahy)'),nl,
    write('Vysledek dotazu "hanoi(2,a,b,c,M)".'),nl,
    write_all_X(hanoi(2,a,b,c,M),M,'M'),
    write('Vysledek dotazu "hanoi(3,a,b,c,M)".'),nl,
    write_all_X(hanoi(3,a,b,c,M),M,'M'),
    write('Vysledek dotazu "hanoi(4,a,b,c,M)".'),nl,
    write_all_X(hanoi(4,a,b,c,M),M,'M').
?-start.

:- retractall(start/0).
:- retractall(write_all_X/3).
