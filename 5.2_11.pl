% nacteni:
/* ['5.2_11.pl']. */

?- op(600, xfx, --->).
?- op(500, xfx, :).

a ---> or:[b,c].
b ---> and:[d,e].
c ---> and:[f,g].
e ---> or:[h].
f ---> and:[h,i].
goal(d).
goal(g).
goal(h).

solve(Node,Node) :- goal(Node).
solve(Node,Node ---> Tree) :-
    Node ---> or:Nodes, member(Node1,Nodes), solve(Node1,Tree).
solve(Node,Node ---> and:Trees) :-
    Node ---> and:Nodes, solveall(Nodes,Trees).

solveall([],[]).
solveall([Node|Nodes],[Tree|Trees]) :- solve(Node,Tree), solveall(Nodes,Trees).


% demonstracni vypis

  % abychom se vyhli varovanim "Redefined static procedure ..."
:- dynamic
       start/0.

start:- 
    write('Prohledavani AND/OR grafu'),nl,nl,
    write('  Graf:'),nl,
    write('    a ---> or:[b,c].'),nl,
    write('    b ---> and:[d,e].'),nl,
    write('    c ---> and:[f,g].'),nl,
    write('    e ---> or:[h].'),nl,
    write('    f ---> and:[h,i].'),nl,
    write('    goal(d).'),nl,
    write('    goal(g).'),nl,
    write('    goal(h).'),nl,nl,
    write('Vysledek dotazu "solve(a,Tree)":'),nl,
    solve(a,Tree),
    write('Tree = '),write(Tree),nl.
    
?-start.

:- retractall(start/0).
