% nacteni:
/* ['6.1_9.pl']. */

:- retractall(start/0).

:- use_module(library(clpfd)). % clpq , clpr

:- dynamic
       start/0.
start:- 
    write('CLP - Constraing Logic Programming'),nl,nl,
    write('vyzkousejte si zadat dotazy:'),nl,
    write('X in 1..5, Y in 2..8, X+Y #= T.'),nl,
    write('X in 1..5, Y in 2..8, X+Y #= T, labeling([],[ X,Y,T]).'),nl,
    write('X #< 4, [X,Y] ins 0..5.'),nl,
    write('X #< 4, indomain(X).'),nl,
    write('X #> 3, X #< 6, indomain(X).'),nl,
    write('X in 4..sup, X #\= 17, fd_dom(X,F).'),nl.
    
?-start.
