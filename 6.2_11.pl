% nacteni:
/* ['6.2_11.pl']. */

:- retractall(start/0).

:- use_module(library(clpfd)). % clpq , clpr

moremoney([S,E,N,D,M,O,R,Y],Type) :- [S,E,N,D,M,O,R,Y] ins 0..9,
                                      S #> 0, M #> 0,
                                      all_different([S,E,N,D,M,O,R,Y]),
                                      sum(S,E,N,D,M,O,R,Y),
                                      labeling(Type, [S,E,N,D,M,O,R,Y]).
                                      
sum(S,E,N,D,M,O,R,Y):-              1000*S + 100*E + 10*N + D
                                  + 1000*M + 100*O + 10*R + E
                       #= 10000*M + 1000*O + 100*N + 10*E + Y.

:- dynamic
       start/0.

start:- 
    write('CLP - Algebrogram'),nl,nl,
    write('  S E N D'),nl,
    write('+ M O R E'),nl,
    write('----------'),nl,
    write('M O N E Y'),nl,nl,
    write('Vysledek dotazu "moremoney([S,E,N,D,M,O,R,Y],[])":'),nl,
    moremoney([S,E,N,D,M,O,R,Y],[]),
     write(' S = '),write(S),write(', E = '),write(E),write(', N = '),write(N),
    write(', D = '),write(D),write(', M = '),write(M),write(', O = '),write(O),
    write(', R = '),write(R),write(', Y = '),write(Y),nl.
    
?-start.
