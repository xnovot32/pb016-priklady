% nacteni:
/* ['8.1_3.pl']. */

% kb_agent_action(+KB,+ATime,+Percept,-Action,-NewATime)
kb_agent_action(KB,ATime,Percept,Action,NewATime):-
    make_percept_sentence(Percept,ATime,Sentence),
    tell(KB,Sentence),             % pridame vysledky pozorovani do KB
    make_action_query(ATime,Query),
    ask(KB,Query,Action),          % zeptame se na dalsi postup
    make_action_sentence(Action,ATime,ASentence),
    tell(KB,ASentence), % pridame informace o akci do KB
    NewATime is ATime + 1.
