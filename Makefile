.PHONY: tests output pylint2 pylint3

IGNORED_PYLINT_TESTS=invalid-name missing-docstring \
	redefined-variable-type too-few-public-methods duplicate-code \
	too-many-locals too-many-branches too-many-arguments \
	too-many-return-statements import-error
IGNORED_PYLINT2_TESTS=superfluous-parens
IGNORED_PYLINT3_TESTS=

tests:
	# all tests
	@make -j 3 output2.7 output3.x pylint pylint3
	# all tests ok

output2.7:
	# python 2.7 output test
	@set -e; \
	TEMP=`mktemp`; \
	trap 'rm $$TEMP' EXIT; \
	for FILE in *.py; do \
		printf '# python 2.7 output test "%s"\n' "$$FILE"; \
		if [ -e "$$FILE.out" ]; then \
			python2.7 "$$FILE" > $$TEMP; \
			diff "$$FILE.out" $$TEMP; \
			printf '# python 2.7 output test "%s" ok\n' "$$FILE"; \
		else \
	    printf 'There exists no output for script %s!' "$$FILE"; \
			exit 1; \
		fi; \
	done
	# python 2.7 output test ok

output3.x:
	# python 3.x output test
	@set -e; \
	TEMP=`mktemp`; \
	trap 'rm $$TEMP' EXIT; \
	for FILE in *.py; do \
		printf '# python 3.x output test "%s"\n' "$$FILE"; \
		if [ -e "$$FILE.out" ]; then \
			python3 "$$FILE" > $$TEMP; \
			diff "$$FILE.out" $$TEMP; \
			printf '# python 3.x output test "%s" ok\n' "$$FILE"; \
		else \
	    printf 'There exists no output for script %s!' "$$FILE"; \
			exit 1; \
		fi; \
	done
	# python 3.x output test ok

COMMA:=,
EMPTY:=
SPACE:=$(EMPTY) $(EMPTY)
PYLINT2_OPTIONS=--disable=$(subst $(SPACE),$(COMMA),\
	$(IGNORED_PYLINT_TESTS) $(IGNORED_PYLINT2_TESTS))
PYLINT3_OPTIONS=--disable=$(subst $(SPACE),$(COMMA),\
	$(IGNORED_PYLINT_TESTS) $(IGNORED_PYLINT3_TESTS))

pylint:
	# pylint test
	@if ! REPORT="$$(pylint $(PYLINT2_OPTIONS) *.py 2>&1)"; then \
		printf "%s\n" "$$REPORT"; \
		exit 1; \
	fi
	# pylint test ok

pylint3:
	# pylint3 test
	@if ! REPORT="$$(pylint3 $(PYLINT3_OPTIONS) *.py 2>&1)"; then \
		printf "%s\n" "$$REPORT"; \
		exit 1; \
	fi
	# pylint3 test ok
