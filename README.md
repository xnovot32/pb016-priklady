Příklady byly odzkoušeny v interpretru Pythonu 2.7.13 a 3.5.3 na Debianu 9 s
Linuxovým jádrem.

Příklady lze spustit příkazem `INTERPRETR NÁZEV_PŘÍKLADU`, kde `INTERPRETR` je
název interpretru Pythonu (typicky `python`, `python2` nebo `python2.7` pro
interpretr Pythonu 2.7.X a `python3` pro interpretr Pythonu 3.X.Y). Výsledný
příkaz může tedy být např. `python 1.1_13.py`.

Většina příkladů vyžaduje, aby byl ve stejné složce stažený i modul
[`linked_lists.py`](linked_lists.py), který definuje třídu představující
spojový seznam ve stylu Lispu.

Jména zdrojových souborů jsou ve tvaru `PŘEDNÁŠKA.PŘÍKLAD_SLIDE.py`.

  * 1\. přednáška
    * [`1.1_13.py`](1.1_13.py) – jednoduchá DB rodinných vztahů
    * [`1.2_16.py`](1.2_16.py) – vypsání seznamu
    * [`1.3.1_18.py`](1.3.1_18.py) – Fibonacciho čísla
    * [`1.3.2_19.py`](1.3.2_19.py) – Fibonacciho čísla, efektivnější varianta
  * 2\. přednáška
    * [`2.1.3_3.py`](2.1.3_3.py) – member III (varianty member I a II jsou
      specifické pro jazyk s unifikací, jako je Prolog)
    * [`2.2_4.py`](2.2_4.py) – del a del1 - výmaz prvku ze seznamu; insert a
      insert1 - vkládání prvku do seznamu
    * [`2.3_5.py`](2.3_5.py) – perm1, 2 a 3 - permutace seznamu
    * [`2.4.1_6.py`](2.4.1_6.py) – append, spojení seznamů
    * [`2.5.1_10.py`](2.5.1_10.py) – QuickSort - třídění seznamů
    * [`2.6_13.py`](2.6_13.py) – addleaf, show - přidávání do binárního stromu,
      výpis binárního stromu
    * [`2.7_15.py`](2.7_15.py) – delleaf - odebírání z binárního stromu
    * [`2.8_17.py`](2.8_17.py) – vícesměrný algoritmus pro vkládání a odebírání
    * [`2.9.1_21.py`](2.9.1_21.py) – cesta v grafu
    * [`2.9.2_22.py`](2.9.2_22.py) – cesta v grafu II
    * [`2.10_23.py`](2.10_23.py) – kostra gafu, vypisuje několikrát stejná
      řešení
  * 3\. přednáška
    * [`3.1_3.py`](3.1_3.py) – problém osmi dam I
    * [`3.2_4.py`](3.2_4.py) – problém osmi dam II - omezení stavového prostoru
    * [`3.3_5.py`](3.3_5.py) – problém osmi dam III - seznamy volných pozic
    * [`3.4_13.py`](3.4_13.py) – prohledávání do hloubky (DFS)
    * [`3.4_13-lifo.py`](3.4_13-lifo.py) – prohledávání do hloubky,
      alternativní implementace pomocí zásobníku
    * [`3.5_15.py`](3.5_15.py) – prohledávání do hloubky s limitem
    * [`3.5_15-lifo.py`](3.5_15-lifo.py) – prohledávání do hloubky s limitem,
      alternativní implementace pomocí zásobníku
    * [`3.6_17.py`](3.6_17.py) – prohledávání do šířky (BFS)
    * [`3.6_17-fifo.py`](3.6_17-fifo.py) – prohledávání do šířky, alternativní
      implementace pomocí fronty
  * 4\. přednáška
    * [`4.1_9.py`](4.1_9.py) – algoritmus A*, pro spuštění je třeba stáhnout i
      modul [`romanian_cities.py`](romanian_cities.py).
    * [`4.1_9-priority-queue.py`](4.1_9-priority-queue.py) – algoritmus A*,
      alternativní implementace pomocí prioritní fronty, pro spuštění je třeba
      stáhnout i modul [`romanian_cities.py`](romanian_cities.py).
    * [`4.1_9-posunovacka.py`](4.1_9-posunovacka.py) – 8-posunovačka,
      heuristiky h1 a h2, pro spuštění je třeba stáhnout i modul
      [`best_search.py`](best_search.py).
    * [`4.2_17.py`](4.2_17.py) – rozvrh práce procesorů, pro spuštění je třeba
      stáhnout i modul [`best_search.py`](best_search.py).
  * 5\. přednáška
    * [`5.1_5.py`](5.1_5.py) – hanoiské věže
    * [`5.2_11.py`](5.2_11.py) – obecné prohledávání AND/OR grafů
    * [`5.3_15.py`](5.3_15.py) – heuristické AND/OR prohledávání
    * [`5.3_15-priority-queue.py`](5.3_15-priority-queue.py) – heuristické
      AND/OR prohledávání, alternativní implementace pomocí prioritní fronty
    * [`5.3_15-cesta_mezi_mesty.py`](5.3_15-cesta_mezi_mesty.py) – příklad
      cesta mezi městy, pro spuštění je třeba stáhnout i modul
      [`and_or_best_search.py`](and_or_best_search.py).
  * 6\. přednáška – pro příklady je zapotřebí nainstalovat pythonovský balíček
    [`python-constraint`](https://github.com/python-constraint/python-constraint).
    * [`6.2_11.py`](6.2_11.py) – algebrogram
    * [`6.3_14.py`](6.3_14.py) – problém N dam
  * 7\. přednáška
    * [`7.1_11.py`](7.1_11.py) – algoritmus minimax
    * [`7.2_15.py`](7.2_15.py) – alfa-beta prořezávání
  * 8\. přednáška
    * [`8.2_20.py`](8.2_20.py) – inference kontrolou modelů
  * 9\. přednáška – žádné příklady
  * 10\. přednáška
    * [`10.1_16.py`](10.1_16.py) – algoritmus IDT
  * 11\. přednáška – žádné příklady
  * 12\. přednáška – žádné příklady
